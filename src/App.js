import './App.css';

import AppNavbar from './components/AppNavbar';
import Home from './pages/Home';
//import Courses from './pages/Courses';
import Products from './pages/Products';
import Register from './pages/Register';
import Login from './pages/Login';
import Logout from './pages/Logout';
import Banner from './components/Banner';
//import CourseView from './components/CourseView';
//import ProductList from './components/ProductList';
import AdminDashboard from './pages/AdminDashboard';
//import ProductView from './components/ProductView';
import ProductSingleView from './components/ProductSingleView';
import OrderList from './components/OrderList';
import OrderView from './components/OrderView';
import UserList from './components/UserList';

import UserView from './components/UserView';
import CartView from './components/CartView';
import CartList from './components/CartList';
//import ErrorPage from './pages/ErrorPage';

import {UserProvider} from './UserContext';
import {Container} from 'react-bootstrap';
import {BrowserRouter as Router, Route, Routes} from 'react-router-dom';
import {useState ,useEffect} from 'react';


export default function App() {

  const [user,setUser]=useState({
    id : null,
    isAdmin : null
  });

  const [intAvail, setAvailability]=useState(1);

  const unsetUser = () => {
    localStorage.clear();
  };

  useEffect(()=>{
    fetch(`${process.env.REACT_APP_API_URL}/users/details`, {
      headers: {
        Authorization: `Bearer ${localStorage.getItem('token')}`
      }
    })
    .then(res => res.json())
    .then(data =>{
      if (typeof data._id !== 'undefined'){
        setUser({ id: data._id, isAdmin: data.isAdmin })
        // console.log(user.id);
        // console.log(user.isAdmin);
      } else {
        setUser({ id: null, isAdmin: null })
      }
    })
  }, []);

  return (
    <>
      <UserProvider value={{user, setUser, unsetUser, intAvail, setAvailability}}>
        <Router>
          < AppNavbar/>
          <Container>
            <Routes>
              < Route path="/" element={<Home/>} />
              < Route path="/products" element={<Products/>} />
              < Route path="/products/:productId" element={<ProductSingleView/>} />
              < Route path="/adminDashboard" element={<AdminDashboard/>} />
              < Route path="/orders" element={<OrderList/>} />
              < Route path="/orderView" element={<OrderView/>} />
              < Route path="/users" element={<UserList/>} />
              < Route path="/userView/:userId" element={<UserView/>} />
              < Route path="/carts" element={<CartList/>} />
              < Route path="/cartView" element={<CartView isRefresh={false}/>} />
              < Route path="/register" element={<Register/>} />
              < Route path="/login" element={<Login/>} />
              < Route path="/logout" element={<Logout/>} />
              {/*< Route path="*" element={<ErrorPage/>} />*/}
              < Route path="*" element={<Banner notFound={true}/>} />
              
            </Routes>
          </Container>
        </Router>
      </UserProvider>
    </>
  );
}

//export default App;
