
//import Swal from 'sweetalert2';

import { useContext} from 'react';
//import {useNavigate} from 'react-router-dom';
import {Navigate,useNavigate} from 'react-router-dom';
import { Table , Button  } from 'react-bootstrap';
import UserContext from '../UserContext';
//import { Row, Col } from 'react-bootstrap';
//import ProductView2 from '../components/ProductView2';
//import Accordion from 'react-bootstrap/Accordion';
import { useLocation } from 'react-router-dom';

export default function OrderView() {

    const {user} = useContext(UserContext);
    const navigate=useNavigate();
    const location = useLocation();

    let data;
    if(location.state!==null){
         data = location.state;
    }else{
         data = null;
    }
    
	return (
        /*<></>*/
        (user.id!==null && user.isAdmin && data.order) ?
        <>
        <h2 className="text-center mt-3"> Order History</h2>
        <Table striped bordered hover>
            <thead>
               <tr>
                <th className="w-25">Customer Name</th>
                <th colSpan={3}> {data.order.userName}</th>
                                                                                
              </tr>
              <tr>
                <th >Purchased Date</th>
                <th colSpan={3}> {new Date(data.order.purchasedOn).toLocaleDateString('en-US')}</th>
                                                                                
              </tr>
              <tr>
                <th >Order #</th>
                <th colSpan={3}> {data.order._id}</th>
                                                                                
              </tr>
              {data.order.cartId !== null &&
                <tr>
                  <th >Cart #</th>
                  <th colSpan={3}> {data.order.cartId}</th>
                </tr>
              }
            </thead>
            
          </Table>

        <Table striped bordered hover>
            <thead>
               
              <tr>
                <th>Product Name</th>
                <th className="w-50">Description</th>
                <th>Quantity</th>
                <th>Price</th>
                <th>Total Price</th>
                
              </tr>
            </thead>
            <tbody>
              {data.order.products.map((product,index) => (
                
                <tr key={`${product.id}-${index}`}>
                  <td>{product.name}</td>
                  <td>{product.description}</td>
                  <td>{product.quantity}</td>
                  <td>{product.price.toLocaleString('en-PH', {style: 'currency', currency: 'PHP'})}</td>
                  <td >{(product.quantity * product.price).toLocaleString('en-PH', {style: 'currency', currency: 'PHP'})}</td>
                  
                </tr>
              ))}
            </tbody>
            <tfoot>
                <tr key={`amount-${data.order.id}`}>
                   <td colSpan={4}>Total Amount :</td>
                   <td className="text-danger">{data.order.totalAmount.toLocaleString('en-PH', {style: 'currency', currency: 'PHP'})}</td>

                </tr>
            </tfoot>
          </Table>

                    
        <Button className="mt-3" variant="primary" onClick={() => navigate("/orders")}>Back</Button>
        </>  
        :<Navigate to="/products"/>
    )
}

