
import Swal from 'sweetalert2';

import {useState, useEffect, useContext} from 'react';
import {useNavigate} from 'react-router-dom';
import {Navigate} from 'react-router-dom';
import { Table , Button  } from 'react-bootstrap';
import UserContext from '../UserContext';
//import { Row, Col } from 'react-bootstrap';
//import ProductView2 from '../components/ProductView2';
//import Accordion from 'react-bootstrap/Accordion';
//import OrderView from './OrderView';


export default function UserList() {

    const {user} = useContext(UserContext);
    const [users, setUsers] = useState([]);
    
    const navigate=useNavigate();

    function showUser(userData){
        //navigate(`/orderView`, { state: {order} });
    }
    function updateUser (userData){
        console.log(userData);
        //e.preventDefault();
        fetch(`${process.env.REACT_APP_API_URL}/users/${userData._id}`,{
            method: 'PUT',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${localStorage.getItem('token')}`
            },
            body: JSON.stringify({
                isAdmin: !userData.isAdmin
            })
        })
        .then((res) => res.json())
        .then((data) => {
            //console.log(data);
            if(!data){
                 Swal.fire({
                    title: "Failed to disable",
                    icon: "warning",
                    text: `${userData._id} `
                })
            }
        });

      
    }

    useEffect(() => {
        if(user.isAdmin){
            fetch(`${process.env.REACT_APP_API_URL}/users/allUserDetails`,{
                headers: {
                    'Authorization': `Bearer ${localStorage.getItem('token')}`
                }
            })
            .then((res) => res.json())
            .then((data) => {
                //console.log(data);
               if(data){
                    setUsers(data);

                }else{
                    setUsers([]);
                }
            });

        }

    });


	return (
        (user.id !== null && user.isAdmin) ?
            <>
            <h2 className="text-center mt-3"> All Users </h2>
            <Table className="mt-2 text-center" striped bordered hover>
                <thead className="bg-dark text-light">
                    <tr>
                        <th className="w-25">First Name</th>
                        <th className="w-25">Last Name</th>
                        <th className="w-25">Email</th>
                        {/*<th>Password</th>*/}
                        <th >Role</th>
                        <th >Action</th>
                    </tr>
                </thead>
                <tbody>
                    {users.map((userData) => (
                        <tr key={userData._id} onDoubleClick={() => showUser(userData)}>
                            <td >{userData.firstName}</td>
                            <td >{userData.lastName}</td>
                            <td >{userData.email}</td>
                            
                            {/*<td >{userData.password}</td>*/}
                            <td >{(userData.isAdmin)?'Admin':''}</td>
                            <td ><Button className="w-100 mb-1" variant="primary" onClick={() => navigate(`/userView/${userData._id}`)}>Edit Details</Button>
                            {(userData.isAdmin)?
                                (user.id===userData._id)?
                                    <Button className="w-100" variant="danger" disabled>Unset Admin</Button>
                                    :<Button className="w-100" variant="danger" onClick={() => updateUser(userData)}>Unset Admin</Button>

                                : <Button className="w-100" variant="warning" onClick={() => updateUser(userData)}>Set Admin</Button>
                            }
                            </td>
                        </tr>
                    ))}
                </tbody>
            </Table>
            </>
        : <Navigate to="/products"/>    



    )
}


