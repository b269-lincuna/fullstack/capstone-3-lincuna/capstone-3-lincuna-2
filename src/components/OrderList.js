
//import Swal from 'sweetalert2';

import {useState, useEffect, useContext} from 'react';
import {useNavigate} from 'react-router-dom';
import {Navigate} from 'react-router-dom';
import { Table , Button  } from 'react-bootstrap';
import UserContext from '../UserContext';
//import { Row, Col } from 'react-bootstrap';
//import ProductView2 from '../components/ProductView2';
import Accordion from 'react-bootstrap/Accordion';
//import OrderView from './OrderView';


export default function OrderList() {

    const {user} = useContext(UserContext);
    const [orders, setOrders] = useState([]);
    
    const navigate=useNavigate();

    function showOrder(order){
        navigate(`/orderView`, { state: {order} });
    }

    useEffect(() => {
        if(!user.isAdmin){
            fetch(`${process.env.REACT_APP_API_URL}/users/showOrder/${user.id}`,{
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization': `Bearer ${localStorage.getItem('token')}`
                }
            })
            .then((res) => res.json())
            .then((data) => {
                //console.log(data);
               if(data){
                    setOrders(data);

                }else{
                    setOrders([]);
                }
            });

        }else{
            fetch(`${process.env.REACT_APP_API_URL}/users/showAllOrders`,{
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization': `Bearer ${localStorage.getItem('token')}`
                }
            })
            .then((res) => res.json())
            .then( async (data) => {

               if(data){

                    let newDataSet=[];

                    for (const dataOrders of data) {
                        let userName = await fetch(`${process.env.REACT_APP_API_URL}/users/userDetails/${dataOrders.userId}`,{
                             headers: {
                                 'Content-Type': 'application/json',
                                 'Authorization': `Bearer ${localStorage.getItem('token')}`
                             }
                         })
                         .then((res2) => res2.json())
                         .then((data3) => {
                             //console.log(data3);
                            if(data3){
                                //console.log(`${data3.lastName}, ${data3.firstName}`)
                                return `${data3.lastName}, ${data3.firstName}`;
                             }
                         });

                        let dataSet={
                            _id: dataOrders._id, 
                            userId: dataOrders.userId, 
                            userName: userName, 
                            cartId:dataOrders.cartId, 
                            products:dataOrders.products,
                            totalAmount:dataOrders.totalAmount, 
                            purchasedOn:dataOrders.purchasedOn
                            }
                        newDataSet.push(dataSet);                     
                    }


                    // console.log("data");
                    // console.log(data);
                    // console.log("newDataSet");
                    // console.log(newDataSet);

                    Promise.all(newDataSet).then((newData) => {
                     setOrders(newData);
                    });

                    //console.log(orders)


                }else{
                    setOrders([]);
                }
            });
        }

    },[user]);


	return (
        (user.id !== null) ?
            (!user.isAdmin)?
            <>
            <h2 className="text-center mt-3"> Order History</h2>
            <Accordion className="mt-3" defaultActiveKey={['0']}>
                {orders.map((order,index) => (
                    <Accordion.Item key={`${order.id}-${index}`} eventKey={index}>
                      
                        <Accordion.Header key={order.id}>{`[ ${new Date(order.purchasedOn).toLocaleDateString('en-US')} ] Total Amount : ${order.totalAmount.toLocaleString('en-PH', {style: 'currency', currency: 'PHP'})} `}
                        </Accordion.Header>
                        <Accordion.Body >
                            
                            <Table striped bordered hover>
                                <thead>

                                <tr>
                                  <th >Purchased Date</th>
                                  <th colSpan={4}> {new Date(order.purchasedOn).toLocaleDateString('en-US')}</th>
                                                                                                  
                                </tr>
                                  <tr>
                                    <th >Order #</th>
                                    <th colSpan={4}>{order._id} </th>
                                                                                                    
                                  </tr>
                                  {order.cartId !== null &&
                                    <tr>
                                      <th >Cart #</th>
                                      <th colSpan={4}>{order.cartId}</th>
                                    </tr>
                                  }
                                  <tr>
                                    <th>Product Name</th>
                                    <th className="w-50">Description</th>
                                    <th>Quantity</th>
                                    <th>Price</th>
                                    <th>Total Price</th>
                                    
                                  </tr>
                                </thead>
                                <tbody>
                                  {order.products.map((product,index) => (
                                    
                                    <tr key={`${product.id}-${index}`}>
                                      <td>{product.name}</td>
                                      <td>{product.description}</td>
                                      <td>{product.quantity}</td>
                                      <td>{product.price.toLocaleString('en-PH', {style: 'currency', currency: 'PHP'})}</td>

                                      <td >{(product.quantity*product.price).toLocaleString('en-PH', {style: 'currency', currency: 'PHP'})}</td>
                                      
                                    </tr>
                                  ))}
                                </tbody>
                                <tfoot>
                                    <tr key={`amount-${order.id}-${index}`}>
                                       <td colSpan={4}><b>Total Amount :</b></td>
                                       <td className="text-danger">{order.totalAmount.toLocaleString('en-PH', {style: 'currency', currency: 'PHP'})}</td>

                                    </tr>
                                </tfoot>
                              </Table>

                        </Accordion.Body>


                    </Accordion.Item>
                ))}
            </Accordion>
            </>
            

            : 
            <>
            <h2 className="text-center mt-3"> Order History</h2>
            <Table className="mt-2 text-center" striped bordered hover>
                <thead className="bg-dark text-light">
                    <tr>
                        <th>Date</th>
                        <th>Customer</th>
                        <th>Order No.</th>
                        <th>Cart No.</th>
                        <th>Total</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>

                    {orders.map((order) => (

                        
                        <tr key={order._id} onDoubleClick={() => showOrder(order)}>
                            <td >{`${new Date(order.purchasedOn).toLocaleDateString('en-US')}`}</td>
                            <td >{order.userName}</td>
                            <td >{order._id}</td>
                            <td >{order.cartId}</td>
                            <td className="text-danger">{order.totalAmount.toLocaleString('en-PH', {style: 'currency', currency: 'PHP'})}</td>
                            <td ><Button className="w-100" variant="primary" onClick={() => showOrder(order)}>Details</Button></td>

                        </tr>
                         
                    ))}
                </tbody>
            </Table>
            </>
        : <Navigate to="/products"/>    



    )
}


