
import {useState, useEffect, useContext} from 'react';
import { Table , Button } from 'react-bootstrap';
import UserContext from '../UserContext';


export default function CartList() {

    const {user} = useContext(UserContext);
    const [carts, setCarts] = useState([{
                            _id: null, 
                            userId: "", 
                            userName: "", 
                            cartId:"", 
                            products:[],
                            cartTotalAmount:0, 
                            placeOrderOn:null
                            }]);
    
    //const navigate=useNavigate();

    function showCart(cart){
        alert("For checking");
        //navigate(`/orderView`, { state: {order} });
    }

    useEffect(() => {
        
            fetch(`${process.env.REACT_APP_API_URL}/carts/showAllCart`,{
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization': `Bearer ${localStorage.getItem('token')}`
                }
            })
            .then((res) => res.json())
            .then( async (data) => {

               if(data){

                    let newDataSet=[];

                    for (const dataOrders of data) {
                        let userName = await fetch(`${process.env.REACT_APP_API_URL}/users/userDetails/${dataOrders.userId}`,{
                             headers: {
                                 'Content-Type': 'application/json',
                                 'Authorization': `Bearer ${localStorage.getItem('token')}`
                             }
                         })
                         .then((res2) => res2.json())
                         .then((data3) => {
                             //console.log(data3);
                            if(data3){
                                //console.log(`${data3.lastName}, ${data3.firstName}`)
                                return `${data3.lastName}, ${data3.firstName}`;
                             }
                         });

                        let dataSet={
                            _id: dataOrders._id, 
                            userId: dataOrders.userId, 
                            userName: userName, 
                            cartId:dataOrders.cartId, 
                            products:dataOrders.products,
                            cartTotalAmount:dataOrders.cartTotalAmount, 
                            placeOrderOn:dataOrders.placeOrderOn
                            }
                        newDataSet.push(dataSet);                     
                    }

                    Promise.all(newDataSet).then((newData) => {
                     setCarts(newData);
                    });

                    //console.log(orders)


                }else{
                    setCarts([{
                            _id: null, 
                            userId: "", 
                            userName: "", 
                            cartId:"", 
                            products:[],
                            cartTotalAmount:0, 
                            placeOrderOn:null
                            }]);
                }
            });
        

    },[user]);


	return (
        (user.id !== null && user.isAdmin) ?
            <>
            <h2 className="text-center mt-3"> Cart History</h2>
                {carts.map((cart,index) => (
                    
                        
                        (cart.products.length>0)?
                        <Table striped bordered hover>
                            <thead className="bg-dark text-light">

                            <tr>
                                <th >Cart #</th>
                                <th colSpan={5}>{cart._id}</th>
                            </tr>

                              <tr>
                                <th >Cart Date</th>
                               {/* <th >Cart #</th>*/}
                                <th>Product Name</th>
                                <th className="w-50">Description</th>
                                <th>Quantity</th>
                                <th>Price</th>
                                <th>Total Price</th>
                                
                              </tr>
                            </thead>

                                <tbody>
                                 
                                  {cart.products.map((product,index) => (
                                    
                                    <tr key={`${product.id}-${index}`}>
                                      <td>{`${new Date(cart.placeOrderOn).toLocaleDateString('en-US')}`}</td>

                                    {/*  <td>{cart._id}</td>*/}
                                      <td>{product.name}</td>
                                      <td>{product.description}</td>
                                      <td>{product.quantity}</td>
                                      <td>{product.price.toLocaleString('en-PH', {style: 'currency', currency: 'PHP'})}</td>

                                      <td >{(product.totalAmount).toLocaleString('en-PH', {style: 'currency', currency: 'PHP'})}</td>
                                      
                                    </tr>
                                  ))}

                                </tbody>
                                <tfoot>
                                    <tr key={`amount-${cart._id}-${index}`}>
                                       <td ><b>Total Amount :</b></td>
                                       <td colSpan={4} className="text-danger">{cart.cartTotalAmount.toLocaleString('en-PH', {style: 'currency', currency: 'PHP'})}</td>
                                       <td ><Button className="w-100" variant="primary" onClick={() => showCart(cart)}>Details</Button></td>
                                    </tr>
                                </tfoot>
                              </Table>
                              :null
                ))}
            </>
            
        : null 



    )
}


/*
<Navigate to="/products"/>  
*/

/*
<>
<h2 className="text-center mt-3"> Order History</h2>
    <Table className="mt-2 text-center" striped bordered hover>
        <thead className="bg-dark text-light">
            <tr>
                <th>Date</th>
                <th>Customer</th>
                <th>Order No.</th>
                <th>Cart No.</th>
                <th>Total</th>
                <th></th>
            </tr>
        </thead>
        <tbody>

            {orders.map((order) => (

                
                <tr key={order._id} onDoubleClick={() => showOrder(order)}>
                    <td >{`${new Date(order.purchasedOn).toLocaleDateString('en-US')}`}</td>
                    <td >{order.userName}</td>
                    <td >{order._id}</td>
                    <td >{order.cartId}</td>
                    <td className="text-danger">{order.totalAmount.toLocaleString('en-PH', {style: 'currency', currency: 'PHP'})}</td>
                    <td ><Button className="w-100" variant="primary" onClick={() => showOrder(order)}>Details</Button></td>

                </tr>
                 
            ))}
        </tbody>
    </Table>
</>  
*/

/*
<h2 className="text-center mt-3"> Cart History</h2>
            <Accordion className="mt-3" defaultActiveKey={['0']}>
                {carts.map((cart,index) => (
                    <Accordion.Item key={`${cart.id}-${index}`} eventKey={index}>
                      
                        <Accordion.Header key={cart.id}>{`[ ${new Date(cart.placeOrderOn).toLocaleDateString('en-US')} ] Total Amount : ${cart.cartTotalAmount.toLocaleString('en-PH', {style: 'currency', currency: 'PHP'})} `}
                        </Accordion.Header>
                        <Accordion.Body >
                            
                            <Table striped bordered hover>
                                <thead>

                                <tr>
                                  <th >Place Order Date</th>
                                  <th colSpan={4}> {new Date(cart.placeOrderOn).toLocaleDateString('en-US')}</th>
                                                                                                  
                                </tr>
                                  <tr>
                                    <th >Cart #</th>
                                    <th colSpan={4}>{cart._id} </th>
                                                                                                    
                                  </tr>
                                 
                                  <tr>
                                    <th>Product Name</th>
                                    <th className="w-50">Description</th>
                                    <th>Quantity</th>
                                    <th>Price</th>
                                    <th>Total Price</th>
                                    
                                  </tr>
                                </thead>
                                <tbody>
                                  {cart.products.map((product,index) => (
                                    
                                    <tr key={`${product.id}-${index}`}>
                                      <td>{product.name}</td>
                                      <td>{product.description}</td>
                                      <td>{product.quantity}</td>
                                      <td>{product.price.toLocaleString('en-PH', {style: 'currency', currency: 'PHP'})}</td>

                                      <td >{(product.totalAmount).toLocaleString('en-PH', {style: 'currency', currency: 'PHP'})}</td>
                                      
                                    </tr>
                                  ))}
                                </tbody>
                                <tfoot>
                                    <tr key={`amount-${cart.id}-${index}`}>
                                       <td colSpan={4}><b>Total Amount :</b></td>
                                       <td className="text-danger">{cart.cartTotalAmount.toLocaleString('en-PH', {style: 'currency', currency: 'PHP'})}</td>

                                    </tr>
                                </tfoot>
                              </Table>

                        </Accordion.Body>


                    </Accordion.Item>
                ))}
            </Accordion>
*/