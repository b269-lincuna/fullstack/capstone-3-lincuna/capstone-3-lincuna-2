
import { useState, useEffect, useContext } from 'react';
import { useParams , useNavigate , Link } from 'react-router-dom';
import { Container, Card, Button, Row, Col } from 'react-bootstrap';
import { Form } from 'react-bootstrap';
import '../css/productView.css'; 
import Swal from 'sweetalert2';

import UserContext from '../UserContext';

export default function ProductView2(props) {
	
	const navigate = useNavigate();

	const {user} = useContext(UserContext);

	//const {productId} = useParams();

	const [name, setName] = useState("");
	const [description, setDescription] = useState("");
	const [price, setPrice] = useState(0);

	function updateProduct (e) {
	    // call the onUpdate prop to update the parent state
	    //props.onSave.callback();
	    e.preventDefault();

	    fetch(`${process.env.REACT_APP_API_URL}/products/updateProduct/${props.onUpdate.productId}`, {
	        method: 'POST',
	        headers: {
	            'Content-Type': 'application/json',
	            'Authorization': `Bearer ${localStorage.getItem('token')}`
	        },
	        body: JSON.stringify({
	            name: name,
	            description: description,
	            price: price
	        })
	    })
	    .then((res) => res.json())
	    .then((data) => {
	    	console.log(data);
	    	if(data) {
	    	    Swal.fire({
	    	        title: "Updated Successfully.",
	    	        icon: "success",
	    	        text: "You updated the product information."
	    	    })
	    	    props.onUpdate.setIsUpdate(false);
    	   }
	    })
	};

	function createProduct (e){
		e.preventDefault();

		fetch(`${process.env.REACT_APP_API_URL}/products/createProduct`, {
		    method: "POST",
		    headers: {
		        'Content-Type': 'application/json',
		        'Authorization': `Bearer ${localStorage.getItem('token')}`
		    },
		    body: JSON.stringify({
		        name: name,
	            description: description,
	            price: price
		    })
		})
		.then(res2 => res2.json())
		.then(data2 => {

		    //console.log(data2)

		    if(data2) {
		        
		        Swal.fire({
					title: "Saved",
					icon: "success",
					text: "Product created."
		        })
		        
		    } else {
		        Swal.fire({
		            title: "Registration Failed",
		            icon: "error",
		            text: "Please try again."
		        })
		    }

		})
	}



	useEffect(() => {
		if(props.onUpdate.productId!==""){
			fetch(`${process.env.REACT_APP_API_URL}/products/${props.onUpdate.productId}`)
			.then(res => res.json())
			.then(data => {
				//console.log(data);
				if(data){
					setName(data.name);
					setDescription(data.description);
					setPrice(data.price);
				} else {
					setName("");
					setDescription("");
					setPrice(0);
				}
			})
		}else{
			setName("");
			setDescription("");
			setPrice(0);
		}
	},[props.onUpdate.productId])//[productId]

	return (

		<Container>
			{(props.onUpdate.isNew && !props.onUpdate.isUpdate)?
					<Row>
						<Col lg={{span: 6, offset:3}} >
					        <Form className="productform mt-3" onSubmit={(e) => createProduct(e)}>
					            <Form.Group controlId="productName">
					                <Form.Label>Product Name</Form.Label>
					                <Form.Control 
					                    type="text" 
					                    as="textarea"
					                    placeholder="Enter Product Name" 
					                    value={name}
					                    onChange={e => setName(e.target.value)}
					                    required
					                />
					            </Form.Group>

					            <Form.Group className="mt-1" controlId="productDescription">
					                <Form.Label>Description</Form.Label>
					                <Form.Control 
					                    //type="text" 
					                    as="textarea"
					                    placeholder="Enter Description" 
					                    value={description}
					                    onChange={e => setDescription(e.target.value)}
					                    required
					                />
					            </Form.Group>

					            <Form.Group className="mt-1" controlId="productPrice">
					                <Form.Label>Price</Form.Label>
					                <Form.Control 
					                    type="number" 
					                    min={0}
					                    placeholder="0" 
					                    value={price}
					                    onChange={e => setPrice(e.target.value)}
					                    required
					                />
					            </Form.Group>

				                <Button variant="primary" type="submit" id="submitBtn" className="my-2 w-50">
				                    CREATE PRODUCT
				                </Button>
					              
					        </Form>
						</Col>
					</Row>
				:
					<Row>
						<Col lg={{span: 6, offset:3}} >
					        <Form className="productform mt-3" onSubmit={(e) => updateProduct(e)}>
					            <Form.Group controlId="productName">
					                <Form.Label>Product Name</Form.Label>
					                <Form.Control 
					                    type="text" 
					                    as="textarea"
					                    //placeholder="Enter first name" 
					                    value={name}
					                    onChange={e => setName(e.target.value)}
					                    required
					                />
					            </Form.Group>

					            <Form.Group className="mt-1" controlId="productDescription">
					                <Form.Label>Description</Form.Label>
					                <Form.Control 
					                    //type="text" 
					                    as="textarea"
					                    //placeholder="Enter last name" 
					                    value={description}
					                    onChange={e => setDescription(e.target.value)}
					                    required
					                />
					            </Form.Group>

					            <Form.Group className="mt-1" controlId="productPrice">
					                <Form.Label>Price</Form.Label>
					                <Form.Control 
					                    type="number" 
					                    min={0}
					                    //placeholder="Enter last name" 
					                    value={price}
					                    onChange={e => setPrice(e.target.value)}
					                    required
					                />
					            </Form.Group>

				                <Button variant="primary" type="submit" id="submitBtn" className="my-2 w-50">
				                    SAVE PRODUCT
				                </Button>
					              
					        </Form>
						</Col>
					</Row>
			}
			
		</Container>

	)
}
