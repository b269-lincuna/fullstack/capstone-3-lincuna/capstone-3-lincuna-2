
import { useState, useEffect,useContext } from 'react';
//import { useState, useEffect, useContext } from 'react';
import { useParams ,Link ,useNavigate, Navigate} from 'react-router-dom';
//import { useParams , useNavigate , Link } from 'react-router-dom';
import { Container, Button } from 'react-bootstrap';
import { Form , Row , Col } from 'react-bootstrap';
import '../css/productView.css'; 
import Swal from 'sweetalert2';
import { Card } from 'react-bootstrap';
import UserContext from '../UserContext';
import { useLocation } from 'react-router-dom';
import CartView from './CartView';

export default function ProductSingleView() {
	
	const location = useLocation();
	const navigate = useNavigate();


    const tempQuantity=location.state

    const [quantity, setQnty]=useState(parseInt(tempQuantity.quantity));
	//let tempQnty=1;
	//const [tempQnty, setTempQnty]=useState(1)

	//const [quantity, setQnty]=useState(0)

	const {user} = useContext(UserContext);

	const {productId} = useParams();
	//const [cart, setCart]=useState({});

	const [name, setName] = useState("");
	const [description, setDescription] = useState("");
	const [price, setPrice] = useState(0);
	//const [totalPrice, setTotalPrice] = useState(0);

	const [isRefresh, setIsRefresh]=useState(false);

	/*function addQnty(e){
		e.preventDefault();
		setTempQnty(e.target.value)
		setTotalPrice(price*tempQnty);
	}*/

	function createOrder (e) {
	    e.preventDefault();

	    fetch(`${process.env.REACT_APP_API_URL}/orders/createOrder`, {
	    	method: "PUT",
	        headers: {
	            'Content-Type': 'application/json',
	            'Authorization': `Bearer ${localStorage.getItem('token')}`
	        },
		    body: JSON.stringify({
		        "products" : [
		                {
		                    "productId": productId,
		                    "quantity" : quantity
		                }
	            ]
		    })
	    })
	    .then((res) => res.json())
	    .then((data) => {
	    	
	    	if(data) {
				// setName(data.name);
				// setDescription(data.description);
				// setPrice(data.price);
		        Swal.fire({
					title: "Order Checked-out!",
					icon: "success",
					text: "Transaction complete."
		        })
		        navigate("/products");
    	   }else {
   		        Swal.fire({
   					title: "Order Checked-out Failed!",
   					icon: "error",
   					text: "Please try again."
   		        })
    	   }
	    })
	};

	function addToCart (e) {
	    e.preventDefault();

	    fetch(`${process.env.REACT_APP_API_URL}/carts/addToCart`, {
	    	method: "POST",
	        headers: {
	            'Content-Type': 'application/json',
	            'Authorization': `Bearer ${localStorage.getItem('token')}`
	        },
		    body: JSON.stringify({
		        "products" : [
		                {
		                    "productId": productId,
		                    "quantity" : quantity
		                }
	            ]
		    })
	    })
	    .then((res) => res.json())
	    .then((data) => {
	    	
	    	if(data) {
		        Swal.fire({
					title: "Added to Cart!",
					icon: "success",
					text: "Transaction complete."
		        })
		        setIsRefresh(!isRefresh);
		        //navigate("/products");
    	   }else {
   		        Swal.fire({
   					title: "Add to cart failed!",
   					icon: "error",
   					text: "Please try again."
   		        })
    	   }
	    })
	};

	useEffect(() => {
		
		fetch(`${process.env.REACT_APP_API_URL}/products/${productId}`)
		.then(res => res.json())
		.then(data => {
			if(data){
				setName(data.name);
				setDescription(data.description);
				setPrice(data.price);
				//setTotalPrice(data.price*quantity);

			} else {
				setName("");
				setDescription("");
				setPrice(0);
			}
		})

		
	},[productId])

	return (
		(!user.isAdmin)?
		<Container>
			<Row>
			<Col lg={{span: 6,}}>
			<Card className="mt-3">
				<Card.Img variant="top" src="holder.js/100px160" />
				<Card.Body>
					<Card.Title>{name}</Card.Title>
					<Card.Subtitle className="text-muted text-wrap my-2" >Description : {description}</Card.Subtitle>
					<Card.Text>Price:  {price}</Card.Text>
					{/*<Card.Text>Price:  {price.toLocaleString('en-PH', {style: 'currency', currency: 'PHP'})}</Card.Text>*/}
					<Form className="d-inline-flex mb-1 ">
						<Card.Text className="mt-2">Qnty: </Card.Text>
						<Card.Text className="text-white mt-2">_</Card.Text>
						<Form.Control className="w-50 py-0"  type="number" placeholder={1} 
						    value={quantity} min={1} onChange={(e) => setQnty(e.target.value)}
						    required
						/>
					</Form>

					<Card.Text >Total:  {(price*quantity).toLocaleString('en-PH', {style: 'currency', currency: 'PHP'})}</Card.Text>
					
					 <Button variant="secondary" as={Link} to={`/products`}>Back</Button>
					 {" "}
					 {(user.id!==null)?
					 	<>
					 		<Button variant="primary" onClick={(e)=> createOrder(e)}>Check-out</Button>
							 {" "}
							 <Button variant="success" onClick={(e)=> addToCart(e)}>Add to cart</Button>
							</>
							:
							<Button variant="danger" onClick={(e)=> navigate("/login")} >Login to Order</Button>
					 }	
							 
					 
					
				</Card.Body>
				{/*<Card.Footer>
					<small className="text-muted">Last updated 3 mins ago</small>
				</Card.Footer>*/}
			</Card>
			</Col>
		</Row>
		<CartView isRefresh={isRefresh}/>
		</Container>
		: <Navigate to="/products"/>
	)
}

/*className="mb-0 mb-sm-1 mb-md-1 mb-lg-0 mb-xl-0 mb-xxl-0" 
<Navigate to="/products"/>
<tr>
  <th >Cart Date</th>
  <th colSpan={4}> {new Date(cart[0].cartDateOn).toLocaleDateString('en-US')}</th>
                                                                  
</tr>

{(user.isAdmin)?
<>
<Button variant="primary" as={Link} to={`/products`}>Back</Button>{" "}
<Button className="mb-0 mb-sm-1 mb-md-1 mb-lg-1 mb-xl-1 mb-xxl-0" 
	    variant="secondary" disabled>Create Order</Button>
{" "}
<Button variant="secondary" disabled>Add to cart</Button>
</>
:
<>
<Button variant="secondary" as={Link} to={`/products`}>Back</Button>{" "}
<Button 
	    variant="primary" as={Link} to={`/`} onClick={(e)=> createOrder(e)}>Create Order</Button>
{" "}
<Button variant="success" as={Link} to={`/`}>Add to cart</Button>
</>
}*/