
import Swal from 'sweetalert2';

import {useState, useEffect, useContext} from 'react';
//import {useNavigate} from 'react-router-dom';
import {Navigate,useNavigate, useParams} from 'react-router-dom';
import { Button, Form  } from 'react-bootstrap';
import UserContext from '../UserContext';
import { Row, Col } from 'react-bootstrap';
//import ProductView2 from '../components/ProductView2';
//import Accordion from 'react-bootstrap/Accordion';
//import { useLocation } from 'react-router-dom';
import FloatingLabel from 'react-bootstrap/FloatingLabel';
import Image from 'react-bootstrap/Image'

export default function UserView() {

    const {user} = useContext(UserContext);
    const navigate=useNavigate();
    

    const {userId} = useParams();

    const [firstName, setFirstName] = useState("");
    const [lastName, setLastName] = useState("");
    const [email, setEmail] = useState("");
    const [isAdmin, setIsAdmin] = useState(false);
    const [password, setPassword] = useState("");

    const [isActive, setIsActive] = useState(false);

    function updateUser(e){
        e.preventDefault();

        fetch(`${process.env.REACT_APP_API_URL}/users/updateUserDetails/${userId}`, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${localStorage.getItem('token')}`
            },
            body: JSON.stringify({
                firstName: firstName,
                lastName: lastName,
                isAdmin: isAdmin
            })
        })
        .then((res) => res.json())
        .then((data) => {
          console.log(data);
          if(data) {
              Swal.fire({
                  title: "Updated Successfully.",
                  icon: "success",
                  text: "You update user information."
              })
              if(user.isAdmin){
                navigate("/users");
              }
              //navigate("/users");
           }
        })
    }

    useEffect(() => {
      
        fetch(`${process.env.REACT_APP_API_URL}/users/userDetails/${userId}`,{
            headers: {
                'Authorization': `Bearer ${localStorage.getItem('token')}`
            }
        })
        .then(res => res.json())
        .then(data => {
          //console.log(data);
          if(data){
            setFirstName(data.firstName);
            setLastName(data.lastName);
            setEmail(data.email);
            setPassword(data.password);
            setIsAdmin(data.isAdmin);
            
          } else {
            setFirstName("");
            setLastName("");
            setEmail("");
            setPassword("");
            setIsAdmin(false);
          }
        })

        
      
    }, [userId])

    useEffect(() => {
      setIsActive(firstName!=="" && lastName!=="");
    }, [firstName,lastName])

	return (
        
        (user.id!==null && user.isAdmin) ?
        <>
        <Row className="mt-3">
            <Col lg={{span: 2}}>
            <Image roundedCircle className="w-100 mt-3" src="https://storage.prompt-hunt.workers.dev/clfdr8bnh0019ml090uynqmm8_2" fluid />
            {(user.id===userId)?
              <>
                <p className="mt-2 mb-0">Hello {firstName} {lastName}!</p>
                <p>Hope your doing well!</p>
              </>
              :
              <>
                <p className="mt-2 mb-0">You are accessing user:</p>
                <p className="mb-0">{firstName} {lastName} information!</p>
                {/*<p>Hope your doing well!</p>*/}
              </>
            }
            
            </Col>
            <Col >
            {/*<Col lg={{span: 6, offset:3}} >*/}
                  <h2 className="my-3"> Edit User Details</h2>
                  <Form className="productform mt-3" onSubmit={(e) => updateUser(e)}>
                      
                      <FloatingLabel controlId="floatingInput1" label="First Name" className="mb-3" >
                        <Form.Control type="text" value={firstName} placeholder="Input First Name" onChange={e => setFirstName(e.target.value)} required/>
                      </FloatingLabel>

                      <FloatingLabel controlId="floatingInput2" label="Last Name" className="mb-3" >
                        <Form.Control type="text" value={lastName} placeholder="Input Last Name" onChange={e => setLastName(e.target.value)} required/>
                      </FloatingLabel>

                      <FloatingLabel controlId="floatingInput3" label="Email address" className="mb-3" >
                        <Form.Control type="email" value={email} placeholder="name@example.com" onChange={e => setEmail(e.target.value)} disabled/>
                      </FloatingLabel>

                      <FloatingLabel controlId="floatingInput4" label="Password" className="mb-3" >
                        <Form.Control type="password" value={password} placeholder="Input Password" onChange={e => setEmail(e.target.value)} disabled/>
                      </FloatingLabel>

                      {(user.id===userId)?
                      <Form.Check type="checkbox" checked={isAdmin} label="Admin" disabled/>
                      :<Form.Check type="checkbox" checked={isAdmin} onChange={() => setIsAdmin(!isAdmin)} label="Admin" />
                      }
                      <div className="text-center">
                          {(isActive)?
                            <Button variant="primary" type="submit" className="my-2 w-25">
                                  SAVE
                              </Button>
                            :
                            <Button variant="danger" className="my-2 w-25" disabled>
                                  SAVE
                              </Button>
                          }{" "}
                          <Button variant="secondary" className="my-2 w-25" onClick={e => navigate("/users")}>
                              BACK
                          </Button>
                        </div>
                        
                  </Form>
            </Col>
          </Row>
        </>  
        :(user.id!==null && !user.isAdmin) ?
        <>
        <Row className="mt-3">
            <Col lg={{span: 2}}>
            <Image roundedCircle className="w-100 mt-3" src="https://storage.prompt-hunt.workers.dev/clfdr8bnh0019ml090uynqmm8_2" fluid />
            <p className="mt-2 mb-0">Hello {firstName} {lastName}!</p>
            <p>Hope your doing well!</p>
            </Col>
            <Col >
            {/*<Col lg={{span: 6, offset:3}} >*/}
                  <h2 className="my-3"> Edit User Details</h2>

                  
                  <Form className="productform mt-3" onSubmit={(e) => updateUser(e)}>
                      
                      <FloatingLabel controlId="floatingInput1" label="First Name" className="mb-3" >
                        <Form.Control type="text" value={firstName} placeholder="Input First Name" onChange={e => setFirstName(e.target.value)} required/>
                      </FloatingLabel>

                      <FloatingLabel controlId="floatingInput2" label="Last Name" className="mb-3" >
                        <Form.Control type="text" value={lastName} placeholder="Input Last Name" onChange={e => setLastName(e.target.value)} required/>
                      </FloatingLabel>

                      <FloatingLabel controlId="floatingInput3" label="Email address" className="mb-3" >
                        <Form.Control type="email" value={email} placeholder="name@example.com" onChange={e => setEmail(e.target.value)} disabled/>
                      </FloatingLabel>

                      <FloatingLabel controlId="floatingInput4" label="Password" className="mb-3" >
                        <Form.Control type="password" value={password} placeholder="Input Password" onChange={e => setEmail(e.target.value)} disabled/>
                      </FloatingLabel>

                      {/*{(user.id===userId)?
                      <Form.Check type="checkbox" checked={isAdmin} label="Admin" disabled/>
                      :<Form.Check type="checkbox" checked={isAdmin} onChange={() => setIsAdmin(!isAdmin)} label="Admin" />
                      }*/}
                      <div className="text-center">
                      {(isActive)?
                        <Button variant="primary" type="submit" className="my-2 w-25">
                              SAVE
                          </Button>
                        :
                        <Button variant="danger" className="my-2 w-25" disabled>
                              SAVE
                          </Button>
                      }
                          

                          {" "}
                          <Button variant="secondary" className="my-2 w-25" onClick={e => navigate("/users")}>
                              BACK
                          </Button>
                        </div>
                        
                  </Form>
            </Col>
          </Row>
        </> 
        :<Navigate to="/products"/>
    )
}

