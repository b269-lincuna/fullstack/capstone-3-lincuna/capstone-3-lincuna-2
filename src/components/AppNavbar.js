

import {Link, NavLink} from 'react-router-dom';
import {useContext} from 'react';

import UserContext from '../UserContext';

import Container from 'react-bootstrap/Container';

import { Nav, Navbar, NavDropdown } from 'react-bootstrap';

export default function AppNavbar() {

  const {user} = useContext(UserContext);

  return (
      <Navbar collapseOnSelect expand="md" bg="dark" variant="dark">
      <Container>
        <Navbar.Brand as={Link} to="/">Techno Shop</Navbar.Brand>
        
        <Navbar.Toggle aria-controls="responsive-navbar-nav" />

        <Navbar.Collapse id="responsive-navbar-nav">
          <Nav className="me-auto">
            <Nav.Link as={NavLink} to="/">Home</Nav.Link>
            <Nav.Link as={NavLink} to="/products">Products</Nav.Link>            
          </Nav>
          
          <Nav>
            {(user.id !== null) ?
               (user.isAdmin) ?
                  <>
                    <NavDropdown title="Admin Dashboard" id="admin-dropdown">
                        <NavDropdown.Item as={Link} to={`/userView/${user.id}`}>Admin Profile</NavDropdown.Item>
                        <NavDropdown.Item as={Link} to="/adminDashboard" >All products</NavDropdown.Item>
                        <NavDropdown.Item as={Link} to="/adminDashboard" >Create Product</NavDropdown.Item>
                        <NavDropdown.Item as={Link} to="/adminDashboard">Update Product</NavDropdown.Item>
                        <NavDropdown.Item as={Link} to="/orders">Order History</NavDropdown.Item>
                        <NavDropdown.Item as={Link} to="/users">All Users</NavDropdown.Item>
                        <NavDropdown.Item as={Link} to="/carts">Cart History</NavDropdown.Item>
                        
                    </NavDropdown>

                    <Nav.Link as={NavLink} to="/logout">Logout</Nav.Link>
                  </>
                : 
                  <>
                    <NavDropdown title="Account" id="account-dropdown">
                        <NavDropdown.Item as={Link} to={`/userView/${user.id}`}>My Profile</NavDropdown.Item>
                        <NavDropdown.Item as={Link} to="/cartView">My Cart</NavDropdown.Item>
                        <NavDropdown.Item as={Link} to="/orders">Order History</NavDropdown.Item>
                    </NavDropdown>

                    <Nav.Link as={NavLink} to="/logout">Logout</Nav.Link>
                  </>
            :
              <>
                <Nav.Link as={NavLink} to="/login">Login</Nav.Link>
                <Nav.Link as={NavLink} to="/register">Register</Nav.Link>
              </>
            }
          </Nav>
        </Navbar.Collapse>
      </Container>
    </Navbar>

  );

}


/*
<Navbar collapseOnSelect expand="md" bg="dark" variant="dark">
      <Container fluid>
        <Navbar.Brand as={Link} to="/">e-Commerce</Navbar.Brand>
        
        <Navbar.Toggle aria-controls="responsive-navbar-nav" />

        <Navbar.Collapse id="responsive-navbar-nav">
          <Nav className="me-auto">
            <Nav.Link as={NavLink} to="/">Home</Nav.Link>
            <Nav.Link as={NavLink} to="/products">Products</Nav.Link>            
          </Nav>
          
          <Nav>
            {(user.id !== null) ?
               (user.isAdmin) ?
                  <>
                    <NavDropdown title="Admin Dashboard" id="admin-dropdown">
                        <NavDropdown.Item as={Link} to="/login">Create Product</NavDropdown.Item>
                        <NavDropdown.Item as={Link} to="/allProducts" >All products</NavDropdown.Item>
                        <NavDropdown.Item as={Link} to="/login">Update Product</NavDropdown.Item>
                      </NavDropdown>

                    <Nav.Link as={NavLink} to="/logout">Logout</Nav.Link>
                  </>
                : 
                  <>
                    <Nav.Link as={NavLink} to="/">Account</Nav.Link>
                    <Nav.Link as={NavLink} to="/logout">Logout</Nav.Link>
                  </>
            :
              <>
                <Nav.Link as={NavLink} to="/login">Login</Nav.Link>
                <Nav.Link as={NavLink} to="/register">Register</Nav.Link>
              </>
            }
          </Nav>
        </Navbar.Collapse>
      </Container>
    </Navbar>
*/

/*<Dropdown as={ButtonGroup}>
  
  <Nav.Link as={NavLink} to="/">Admin Dashboard</Nav.Link>

  <Dropdown.Toggle variant="dark" />
  <Dropdown.Menu variant="dark">
    <Dropdown.Item as={Link} to="/login">Create Product</Dropdown.Item>
    <Dropdown.Item as={Link} to="/allProducts" >All products</Dropdown.Item>
    <Dropdown.Item as={Link} to="/login">Update Product</Dropdown.Item>
    
  </Dropdown.Menu>

</Dropdown>*/

/*
<Accordion>
                      <Accordion.Toggle as={Nav.Link} variant="dark" eventKey="0">
                        Admin Dashboard
                      </Accordion.Toggle>
                      <Accordion.Collapse eventKey="0">
                        <Nav>
                          <Nav.Link as={Link} to="/login">Create Product</Nav.Link>
                          <Nav.Link as={Link} to="/allProducts" >All products</Nav.Link>
                          <Nav.Link as={Link} to="/login">Update Product</Nav.Link>
                          
                        </Nav>
                      </Accordion.Collapse>
                    </Accordion>
*/

/*<>
<Nav.Link as={NavLink} to="/logout">Logout</Nav.Link>
</>*/

/*
//<NavDropdown title="Admin Dashboard" id="admin-dropdown" as={Link} to="/products">
                      <NavDropdown title="Admin Dashboard" id="admin-dropdown">
                        <NavDropdown.Item as={Link} to="/login">Create Product</NavDropdown.Item>
                        <NavDropdown.Item as={Link} to="/allProducts">All products</NavDropdown.Item>
                        <NavDropdown.Item as={Link} to="/login">Update Product</NavDropdown.Item>
                        <NavDropdown.Item as={Link} to="/login">Deactivate/reactivate product</NavDropdown.Item>
                      </NavDropdown>
*/



/*

import Accordion from 'react-bootstrap/Accordion';
import { useAccordionButton } from 'react-bootstrap/AccordionButton';
import Card from 'react-bootstrap/Card';

function CustomToggle({ children, eventKey }) {
  const decoratedOnClick = useAccordionButton(eventKey, () =>
    console.log('totally custom!'),
  );

  return (
    <button
      type="button"
      style={{ backgroundColor: 'pink' }}
      onClick={decoratedOnClick}
    >
      {children}
    </button>
  );
}

function Example() {
  return (
    <Accordion defaultActiveKey="0">
      <Card>
        <Card.Header>
          <CustomToggle eventKey="0">Click me!</CustomToggle>
        </Card.Header>
        <Accordion.Collapse eventKey="0">
          <Card.Body>Hello! I'm the body</Card.Body>
        </Accordion.Collapse>
      </Card>
      
    </Accordion>
  );
}

render(<Example />);
*/