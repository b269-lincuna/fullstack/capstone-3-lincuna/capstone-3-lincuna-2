
import Swal from 'sweetalert2';

import {useState, useEffect, useContext} from 'react';
//import {useNavigate} from 'react-router-dom';
import {Navigate} from 'react-router-dom';
import { Table , Button, Form  } from 'react-bootstrap';
import UserContext from '../UserContext';
//import { Row, Col } from 'react-bootstrap';
//import ProductView2 from '../components/ProductView2';

export default function ProductList(props) {

    //const {user} = useContext(UserContext);
    /*const [testAvail, setTestAvail]=useState(1)*/
    //const [intAvail, setAvailability]=useState(1)
    const {user,intAvail, setAvailability} = useContext(UserContext);
    const [products, setProducts] = useState([]);
    
    //let navigate=useNavigate();

    function updateProduct (product){
        //e.preventDefault();
        fetch(`${process.env.REACT_APP_API_URL}/products/${product._id}`,{
            method: 'PATCH',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${localStorage.getItem('token')}`
            },
            body: JSON.stringify({
                isActive: !product.isActive
            })
        })
        .then((res) => res.json())
        .then((data) => {
            //console.log(data);
            if(!data){
                 Swal.fire({
                    title: "Failed to disable",
                    icon: "warning",
                    text: `${product._id}: ${product.name} `
                })
            }
        });

      
    }

    const viewProduct = (productId) => {
        if(productId===props.onEdit.productId){
            props.onEdit.setProductId("");
            props.onEdit.setIsUpdate(false);
        }else{
            props.onEdit.setIsUpdate(true);
            props.onEdit.setProductId(productId);
            //props.onEdit.scrollToRef();
            //navigate="/adminDashboard#my-section"
            //<Navigate to="/adminDashboard#my-section"/>
        }
        props.onEdit.setIsNew(false);
      };

   /* const setAvail = (avail)=>{
        setAvailability(avail);
        
    }*/

    useEffect(() => {
        fetch(`${process.env.REACT_APP_API_URL}/products/showProducts`,{
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${localStorage.getItem('token')}`
            },
            body: JSON.stringify({
                avail: intAvail
            })
        })
        .then((res) => res.json())
        .then((data) => {
            //console.log(data);
           setProducts(data);
        });

    });


	return (
        (user.isAdmin)?
        <>
        <div className="mt-2 text-center">
        <Form >
           {/* <Button className="w-25 " variant={(testAvail===0)?"primary":"secondary"} onClick={() => setTestAvail(0)}>ALL PRODUCTS</Button>
            <Button className="mx-2 w-25 " variant={(testAvail===1)?"primary":"secondary"} onClick={() => setTestAvail(1)}>ACTIVE PRODUCTS</Button>
            <Button className="w-25 "variant={(testAvail===2)?"primary":"secondary"} onClick={() => setTestAvail(2)}>INACTIVE PRODUCTS</Button>*/}
            <Button className="w-25 " variant={(intAvail===0)?"primary":"secondary"} onClick={() => setAvailability(0)}>ALL PRODUCTS</Button>
            <Button className="mx-2 w-25 " variant={(intAvail===1)?"primary":"secondary"} onClick={() => setAvailability(1)}>ACTIVE PRODUCTS</Button>
            <Button className="w-25 "variant={(intAvail===2)?"primary":"secondary"} onClick={() => setAvailability(2)}>INACTIVE PRODUCTS</Button>
        </Form>
        </div>
        <Table className="mt-2" striped bordered hover>
            <thead className="bg-dark text-light text-center">
                <tr>
                    <th>Name</th>
                    <th>Description</th>
                    <th>Price</th>
                    <th>Availability</th>
                    <th>Actions</th>
                </tr>
            </thead>
            <tbody>

                {products.map((product) => (

                    
                    <tr key={product._id}>
                        <td className="text-center">{
                            (product.name.length<30)? product.name
                               : `${product.name.substring(0, 27)}...`
                        }</td>
                        <td className="w-75">{product.description}</td>
                        <td className="text-center">{product.price.toLocaleString('en-PH', {style: 'currency', currency: 'PHP'})}</td>
                        <td className="text-center">{(product.isActive)? 'Available':'N/A'}</td>
                        <td >
                            <Button className="w-100 mt-1 mb-1" variant="primary" onClick={() => viewProduct(product._id)}>Edit</Button>{" "}
                           
                            {(product.isActive)?
                                <Button className="w-100" variant="danger" onClick={() => updateProduct(product)}>Disable</Button>
                                : <Button className="w-100" variant="warning" onClick={() => updateProduct(product)}>Enable</Button>
                            }
                        </td>
                    </tr>
                     
                ))}
            </tbody>
        </Table>
        </>
        : <Navigate to="/products"/>
    )
}