
//import {useState, useEffect} from 'react';
import {useNavigate} from 'react-router-dom';
//import {Navigate} from 'react-router-dom';
//import {Navigate}
import { Button, Card  } from 'react-bootstrap';
import { Form } from 'react-bootstrap';
//import { Button, Row, Col, Card } from 'react-bootstrap';
//import { CardColumns } from 'react-bootstrap';
import {useState, useContext } from 'react';
import UserContext from '../UserContext';

//import OverlayTrigger from 'react-bootstrap/OverlayTrigger';
//import Tooltip from 'react-bootstrap/Tooltip';

export default function ProductCard({product}) {

    const {user} = useContext(UserContext);
    const navigate = useNavigate();
    // Destructure
    //const {name, description, price, _id } = product;
    const {name, description, price } = product;
    const [quantity, setQuantity]=useState(1);

    function handleCheckout(productId) {
        navigate(`/products/${productId}`, { state: { quantity } });
      }

    return (

        <>
            
            <Card className="cardHighlight p-0 h-100">
                <Card.Body className="d-flex flex-column">

                   
                    <div className="mb-1">

                        <Card.Title ><h4>{name}</h4></Card.Title>

                        <Card.Text>- {
                            ((description.length>100)?
                              `${description.substring(0, 97)}...`
                              : `${description} `)
                        } 
                        </Card.Text>
                       
                    </div>
                     
                    <div className="mt-auto">
                        
                        <Card.Text >Price: {price.toLocaleString('en-PH', {style: 'currency', currency: 'PHP'})}</Card.Text>
                          <Form className=" mb-1">
                              {/*<Form.Group controlId={`quantity-${product._id}`} className="d-flex">*/}
                                  <div className="d-flex">
                                  <Form.Label className="mt-2">Quantity:</Form.Label>
                                  <Form.Label className="mt-2 text-white">_</Form.Label>
                                  <Form.Control className="w-50 py-0" type="number" placeholder={0} 
                                      value={quantity} min={1} onChange={e => setQuantity(e.target.value)}
                                      required
                                  />
                                  </div>
                              {/*</Form.Group>*/}
                          </Form>
                        
                        {(user.isAdmin)?
                            <>
                                <Button className="mb-0 mb-sm-1 mb-md-1 mb-lg-1 mb-xl-1 mb-xxl-0" 
                                variant="primary" disabled>Create Order</Button>
                                {" "}
                                <Button variant="success" disabled>Add to cart</Button>

                            </>
                            :
                            <>
                              <div >
                                <Button className="w-100 mb-0 mb-sm-1 mb-md-1 mb-lg-1 mb-xl-1 mb-xxl-0 "
                                variant="primary" onClick={() => handleCheckout(product._id)}>Create Order</Button>
                                {/*{" "}
                                <Button className="mb-0 mb-sm-1 mb-md-1 mb-lg-1 mb-xl-1 mb-xxl-0 " variant="success" onClick={() => handleCheckout(product._id)}>Add to cart</Button>
                                */}
                              </div>
                            </>
                        }
                    </div>
                </Card.Body>

            </Card>
            
        </>
    )
}

/*

<OverlayTrigger key={'bottom'} placement={'bottom'} overlay={
                                    <Tooltip id={`tooltip-bottom`}>
                                    Click for more details.
                                    </Tooltip>
                                }>
                                <Button className="mb-0 mb-sm-1 mb-md-1 mb-lg-1 mb-xl-1 mb-xxl-0 " variant="secondary" >>></Button>
                                </OverlayTrigger>

className="d-flex"
w-100 w-lg-25 w-xl-25 w-xxl-25
w-100 w-lg-25 w-xl-25 w-xxl-25
w-100 w-lg-25 w-xl-25 w-xxl-25

delay={{ show: 250, hide: 300 }}

*/
