
import Swal from 'sweetalert2';

import {useState, useEffect, useContext} from 'react';
//import {useNavigate} from 'react-router-dom';
import {Navigate,useNavigate} from 'react-router-dom';
import { Table , Button  } from 'react-bootstrap';
import UserContext from '../UserContext';
import { Row, Col } from 'react-bootstrap';
//import ProductView2 from '../components/ProductView2';
//import Accordion from 'react-bootstrap/Accordion';
//import { useLocation } from 'react-router-dom';

export default function CartView(isRefresh) {

    const {user} = useContext(UserContext);
    const navigate=useNavigate();
    
    const [cart, setCart]=useState({
      _id:null,
      products:[{
        name:"",
        description: "",
        quantity:0,
        price:0,
        productId:0
      }
      ],
      cartTotalAmount:0
    })

    //const[qnty, setQnty]=useState(0);

    const [loading, setLoading] = useState(true);
    //const location = useLocation();
   
    
    function removeToCart(productId){
      //console.log(productId)
      fetch(`${process.env.REACT_APP_API_URL}/carts/removeFromCart/${productId}`,{
        method:'PATCH',
        headers: {
          'Content-Type': 'application/json',
          'Authorization': `Bearer ${localStorage.getItem('token')}`
        }
      })
      .then(res => res.json())
      .then(data => {
        if (data) {
          //console.log("removed")
            fetch(`${process.env.REACT_APP_API_URL}/carts/showCart/${user.id}`,{
              headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${localStorage.getItem('token')}`
              }
            })
            .then(res => res.json())
            .then(data => {
             
              if (data) {
                if(data.products.length>0){
                  setCart(data); 
                }else{
                    setCart({
                      _id:null,
                      products:[{
                        name:"",
                        description: "",
                        quantity:0,
                        price:0,
                        productId:0
                      }
                      ],
                      cartTotalAmount:0
                    });
                }
                // console.log("cart");
                // console.log(cart);
              } else { 
                setCart({});
              }  
              //setLoading(false)
            })
        } 
      })
    }

    function checkOutProd(productId){
      alert('for checking')
    }

    function checkOutCart(cartId){
      
      fetch(`${process.env.REACT_APP_API_URL}/carts/checkOut/${cartId}`,{
        method:'PUT',
        headers: {
          'Content-Type': 'application/json',
          'Authorization': `Bearer ${localStorage.getItem('token')}`
        }
      })
      .then(data => {
        if (data) {
          //console.log("checked out")

            Swal.fire({
              title: "Checked-out Successfully",
              icon: "success",
              text: "Cart has beed checked-out."
            })

            navigate("/orders");

            fetch(`${process.env.REACT_APP_API_URL}/carts/showCart/${user.id}`,{
              headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${localStorage.getItem('token')}`
              }
            })
            .then(res => res.json())
            .then(data => {
              if (data) {
                if(data.products.length>0){
                    setCart(data); 
                }else{
                    setCart({
                      _id:null,
                      products:[{
                        name:"",
                        description: "",
                        quantity:0,
                        price:0,
                        productId:0
                      }
                      ],
                      cartTotalAmount:0
                    });
                }
              } else { 
                setCart({
                      _id:null,
                      products:[{
                        name:"",
                        description: "",
                        quantity:0,
                        price:0,
                        productId:0
                      }
                      ],
                      cartTotalAmount:0
                    });
              }  
              //setLoading(false)
            })
        } 
      })
    }

    function changeQnty(productId, productQnty){
        if(productQnty>0){
          fetch(`${process.env.REACT_APP_API_URL}/carts/updateQuantity/${productId}`,{
            method:'POST',
            headers: {
              'Content-Type': 'application/json',
              'Authorization': `Bearer ${localStorage.getItem('token')}`
            },
            body: JSON.stringify({
                quantity : productQnty
            })
          })
          .then(data => {
           
              //console.log("change qnty")

            if (data) {
                fetch(`${process.env.REACT_APP_API_URL}/carts/showCart/${user.id}`,{
                  headers: {
                    'Content-Type': 'application/json',
                    'Authorization': `Bearer ${localStorage.getItem('token')}`
                  }
                })
                .then(res => res.json())
                .then(data => {
                  if (data) {
                    if(data.products.length>0){
                        setCart(data); 
                    }else{
                        setCart({
                          _id:null,
                          products:[{
                            name:"",
                            description: "",
                            quantity:0,
                            price:0,
                            productId:0
                          }
                          ],
                          cartTotalAmount:0
                        });
                    }
                  } else { 
                    setCart({
                          _id:null,
                          products:[{
                            name:"",
                            description: "",
                            quantity:0,
                            price:0,
                            productId:0
                          }
                          ],
                          cartTotalAmount:0
                        });
                  }  
                  //setLoading(false)
                })
            }  
          })
        }else{
          Swal.fire({
              title: "Minimum of 1 in quantity is required ",
              icon: "warning",
              text: "Please remove the product if you want to set the quantity to zero."
            })
        }
    }

    useEffect(() => {
        setLoading(true);
        fetch(`${process.env.REACT_APP_API_URL}/carts/showCart/${user.id}`,{
          headers: {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${localStorage.getItem('token')}`
          }
        })
        .then(res => res.json())
        .then(data => {
          //console.log(data);
          if (data) {
                if(data.products.length>0){
                    setCart(data); 
                }else{
                    setCart({
                      _id:null,
                      products:[{
                        name:"",
                        description: "",
                        quantity:0,
                        price:0,
                        productId:0
                      }
                      ],
                      cartTotalAmount:0
                    });
                }
                

                // console.log("cart");
                // console.log(cart);
          } else { 
            setCart({
              _id:null,
              products:[{
                name:"",
                description: "",
                quantity:0,
                price:0,
                productId:0
              }
              ],
              cartTotalAmount:0
            });
          }  
          setLoading(false)
        })
    },[user,isRefresh])

    if (loading) {
      //console.log(loading);
      return null; // render loading indicator while waiting for data
      /*return <p>Loading...</p>; // render loading indicator while waiting for data*/
    }
    // console.log(cart);

	return (
        /*<></>*/
        (user.id!==null && !user.isAdmin && cart._id!==null) ?
          <>
          {/*<h2 className="mt-3"> Cart Details</h2>*/}
          <Row>
          <Col lg={{span: 12}}>
            <Table striped bordered hover className="mt-3">
                <thead>
                  <tr>
                    <th colSpan={6} ><h2>Cart Details</h2></th>
                  </tr>
                  <tr>
                    
                    <th >Cart #</th>
                    {/*<th colSpan={4}>Cart #</th>*/}
                    <th colSpan={5}>{cart._id} </th>
                    
                  </tr>
                  
                  <tr className="text-center">
                    <th>Product Name</th>
                    <th className="w-50">Description</th>
                    <th >Quantity</th>
                    <th>Price</th>
                    <th>Total Price</th>
                    <th>Action</th>
                    
                  </tr>
                </thead>
                <tbody>
                  
                  {cart._id!==null && cart.products.map((product,index) => (
                    
                    <tr key={`${product.id}-${index}`}>
                      <td>{product.name}</td>
                      <td>{product.description}</td>

                      <td>
                        <Button className="text-center px-2" variant="primary" 
                            onClick={()=> changeQnty(product.productId, (product.quantity-1))}>-</Button>
                        {" "}{product.quantity}{" "}
                        <Button className="text-center px-1" variant="primary" 
                            onClick={()=> changeQnty(product.productId, (product.quantity+1))}>+</Button></td>


                      <td>{(product.price).toLocaleString('en-PH', {style: 'currency', currency: 'PHP'})}</td>

                      <td >{
                        (product.quantity*product.price)
                        .toLocaleString('en-PH', {style: 'currency', currency: 'PHP'})}</td>
                      
                      <td>
                      <Button className="w-100 mb-1" variant="primary" 
                            onClick={()=> checkOutProd(product.productId)}>Checkout</Button>
                      <Button className="w-100" variant="danger" 
                            onClick={()=> removeToCart(product.productId)}>Remove</Button>
                      </td>
                    </tr>
                  ))}
                  
                
                </tbody>
                <tfoot>
                    <tr key={`amount-${cart._id}`}>
                       <td colSpan={4}><b>Total Amount :</b></td>
                       
                       <td className="text-danger"> {(cart.cartTotalAmount).toLocaleString('en-PH', {style: 'currency', currency: 'PHP'})}</td>
                       <td>
                          {cart._id!==null && <Button className="w-100 m-0" variant="warning" 
                                                      onClick={()=> checkOutCart(cart._id)}>Checkout Cart</Button>}
                      </td>
                    </tr>
                    
                </tfoot>
              </Table>

               
          </Col>
          </Row>
        </>
        :
                (cart._id==null && !user.isAdmin)? <h2 className="mt-4"> Cart is Empty.</h2>
                  :<Navigate to="/products"/>
    )
}

/* <Button className="mt-3" variant="primary" onClick={() => navigate("/orders")}>Back</Button>*/

/*
:
        (cart._id==null && !user.isAdmin)? <h2 className="mt-4"> No Cart Found</h2>
          :<Navigate to="/products"/>

*/