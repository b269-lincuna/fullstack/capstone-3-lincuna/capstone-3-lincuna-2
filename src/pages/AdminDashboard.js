
import ProductList from '../components/ProductList';
import ProductView from '../components/ProductView';
import {useState,useContext } from 'react';
import UserContext from '../UserContext';
import {Navigate, useNavigate} from 'react-router-dom';
import { Button} from 'react-bootstrap';

export default function AdminDashboard() {

	const {user} = useContext(UserContext);
	const [productId, setProductId]= useState([]);
	const [isUpdate, setIsUpdate]= useState(false);
	const [isNew, setIsNew]= useState(false);
	const navigate=useNavigate();

	const setNew = ()=>{
		if(!isNew && !isUpdate){
			setIsNew(true);
		}
		else if(!isNew && isUpdate){
			
			setIsUpdate(false);
			setProductId("");
			setIsNew(true);
		}
		else{
			setIsNew(false);
			setIsUpdate(false);
			setProductId("");
		}
	}

return (

		(user.isAdmin)?

	    	<>
	    	<h3 className="text-center mt-3">ADMIN DASHBOARD</h3>
	    	<div className="mt-3 text-center" >
		    	<Button className="w-25"  variant="success" size="lg" onClick={() => setNew()}>
		          	Add New Product
		        </Button>{' '}
		        <Button className="w-25" variant="primary" size="lg" onClick={() => navigate("/orders")}>
		          	Show Users Orders
		        </Button>
	        </div>

	    	{(isNew || isUpdate) &&
	    		<ProductView id="createSection" onUpdate={{productId: productId, setProductId:setProductId, isNew:isNew, setIsNew: setIsNew, isUpdate:isUpdate, setIsUpdate: setIsUpdate}}/>}
	    	
	    	<ProductList onEdit={{productId: productId, setProductId:setProductId, isNew:isNew, setIsNew: setIsNew,isUpdate:isUpdate, setIsUpdate: setIsUpdate}}/>
	    	</>
	    : <Navigate to="/products"/>

	)
}

/*<ProductView onUpdate={{productId: productId,setUpdate:setUpdate}}/>}*/
/*<ProductView onUpdate={{productId: productId,isNew:isNew, setIsNew: setIsNew, isUpdate:isUpdate, setIsUpdate: setIsUpdate}}/>*/