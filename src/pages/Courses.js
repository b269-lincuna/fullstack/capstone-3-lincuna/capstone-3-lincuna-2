//https://react-bootstrap.github.io/components/cards/

import {useState, useEffect, useContext} from 'react';

//import coursesData from '../data/coursesData';
import CourseCard from '../components/CourseCard';


export default function Courses(){
	//console.log(coursesData);

	// const courses = coursesData.map((course)=>{
	// 	return (
	// 		< CourseCard key={course.id} course = {course}/>
	// 	)
	// })

	//console.log(courses);

	// id : null,
	// name : null,
	// description : null,
	// price : null,
	// isActive : null,
	// enrollees : null
	
	const [courses, setCourses]=useState([]);
	

	useEffect(() => {
		fetch(`${process.env.REACT_APP_API_URL}/courses/active`)
		.then(res => res.json())
		.then(data => {
			console.log(data);
			setCourses(data.map(course => {
				return(
					<CourseCard key={course._id} course={course} />
				)
			}))
		})
	}, [])

	return (
		<>
		{courses}
		</>
	)
}
